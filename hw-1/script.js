const menu = document.querySelector('.menu');
const burger = document.querySelector('.burger');
const nav = document.querySelector('.nav');
const body = document.querySelector('body');
const map = document.querySelector('.map-wrapper');
let expanded = false;
menu.addEventListener('click', expand);

function expand () {
    if (expanded === true) {
        map.classList.remove('hidden');
        nav.classList.remove('nav-active');
        burger.classList.remove('hidden');
        menu.classList.remove('menu-active');
        body.classList.remove('no-scroll');
        expanded = false;

    } else {
        map.classList.add('hidden');
        nav.classList.add('nav-active');
        burger.classList.add('hidden');
        menu.classList.add('menu-active');
        body.classList.add('no-scroll');
        expanded = true;
    }
}



